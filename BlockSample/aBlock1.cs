﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using a.Core;
using a.b.Arrays;
using c.Configuration;

namespace BlockSample
{
    public class aBlock1 : aBlock
    {
        public aEnumerableAtom<object> Input { get; protected set; }
        public aEnumerableAtom<object> Result { get; set; }
    }
}
