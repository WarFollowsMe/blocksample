﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using a.Core;
using a.b.Arrays;
using c.Configuration;


namespace BlockSample
{
    /// <summary>
    /// Пример блока. Этот блок фильтрует входящий массив данных по какому-то условию.
    /// Для опрежеления что данный класс является блоком его 
    /// 1) надо отнаследовать от класса aBlock (или от класса-наследника родителем которого является aBlock)
    /// 2) указать аттрибут aBlock c аргументами DisplayName, Supply, Type, Description, FullDescription, Aythor, Version, Year, Month, Day
    /// </summary>
    [aBlock("Фильтр", "Массивы", "Where", "Where", "Применяет к массиву заданное условие выборки", "vaa", 1, 2015, 08, 08)]
    public class ArrayWhere : aBlock1
    {
        /// <summary>
        /// Конфигурационный параметр. Задается на этапе конфигурирования блока.
        /// Все конфигурационные параметры помечаются аттрибутом cSetter
        /// с аргументами description (понятное название, оно отображается в системе)
        /// и name (имя параметра). Могут совпадать.
        /// </summary>
        [cSetter("Условие", "Условие")]
        public string Condition { get; set; }

        /*
         * Дальше создадим пару атомов.
         * По сути блок это обертка над некоторой алгоритмикой
         * И он не несет какой-либо информации
         * Вся информация передается через АТОМЫ. 
         * По типу доступа атомы разделяются на три типа:
         * Write  (входной атом, может быть изменен извне блока, и при его изменении блок выполняет свою алгоритмику
         * Update (выходной атом. В атомы этого типа записываются результаты выполнения алгоритмики
         * WriteAndUpdate (и входной и выходной атом)
         */
        

        public ArrayWhere()
        {
            /*
             * Определяем объекты атомов
             * Input - входной атом, aAtomAccess = Write
             * Result - выходной атом, соответственно Update
             * Надеюсь логика понятна
             */ 
            Input = new aEnumerableAtom<object>("inputarray", aAtomAccess.Write);
            Result = new aEnumerableAtom<object>("result", aAtomAccess.Update); 
            
            /*
             * задаем значение по умолчанию для нашего параметра конфигурации
             */
            Condition = "Convert.ToInt32(it) > 10";
        }

        /// <summary>
        /// Это обработчик события Конфигурации. 
        /// Вообще у блока есть несколько состояний:
        /// ready (в этом состоянии можно менять конфигурационные параметры),
        /// alive (рабочее состояние, отслеживает значения входящих атомов, если они изменились проивзводит расчет заданной алгоритмики,
        /// starting,stoping (запуск, остановка - промежуточные этапы между ready и alive)
        /// Если в состоянии ready конфигурация блока изменилась то вызывается событие конфигурации,
        /// метод internal_Configurate является обработчиком жанного события.
        /// Здесь происходит подготовка блкоа к исполнению алгоритмов, назвначение каких-либо 
        /// условий, уставок или параметров
        /// В данном случае это только параметр Condition.
        /// </summary>
        protected override void internal_Configurate()
        {
            /*
             * Делаем проверку не пустое ли условие. В данном случае без паарметра Condition блок работать не может, 
             * поэтому если параметр не задан выкидываем ошибку с соответсвующим сообщением.
             */
            if (string.IsNullOrEmpty(Condition)) throw new aConfigurateException(this, "Не задано условие выборки");
        }
        /// <summary>
        /// Этот обработчик события изменения входных атомов
        /// </summary>
        /// <param name="CouponObject"></param>
        /// <param name="writingValues">массив ксерокопий измененных атомов. Каждый атом содержит Значение, 
        /// Качество и Метку времени. Ксерокопия атома - это снимок этих трех параметров</param>
        /// <returns></returns>
        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            /*
             * Для того чтобы применить изменение атома и значение входного атома установилось в новое,
             * необходимо вызвать у атома метод Set с ксерокопией несущей новое значение.
             * foreach (var a in writingValues) 
                a._A.Set(a);
             * Это уже по умолчанию заложено в классе aBlock, поэтому если нет необходимости
             * делать проверку на изменненое значение, то можно просто вызвать 
             * base.internal_Write(CouponObject, writingValues);
             */
            base.internal_Write(CouponObject, writingValues);
            var resultValue = Input.value.AsQueryable<object>().Where(Condition);
            Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
            return new aAtom[] { Result };
        }
    }


    //UNION
    [aBlock("Объединение", "Массивы", "Union", "Union","Объединяет массивы", "Shirokov Ivan", 1, 2015, 08, 09)]
    public class ArrayUnion : aBlock1
    {
        public aEnumerableAtom<object> Input2 { get; protected set; }
        
        public ArrayUnion()
        {
            Input = new aEnumerableAtom<object>("inputArray1", aAtomAccess.Write);
            Input2 = new aEnumerableAtom<object>("inputArray2", aAtomAccess.Write);
            Result = new aEnumerableAtom<object>("result", aAtomAccess.Update);
        }
        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            base.internal_Write(CouponObject, writingValues);
            var resultValue = Input.value.AsQueryable<object>().Union<object>(Input2.value);
            Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
            return new aAtom[] { Result };
        }
    }


    //TAKE
    [aBlock("Взятие", "Массивы", "Take", "Take", "Возвращает указанное число элементов", "Shirokov Ivan", 1, 2015, 08, 09)]
    public class ArrayTake : aBlock1
    {
        [cSetter("Число элементов","Число элементов")]
        public int number {get; protected set;}
        
        public ArrayTake()
        {
            Input = new aEnumerableAtom<object>("inputArray", aAtomAccess.Write);
            Result = new aEnumerableAtom<object>("result", aAtomAccess.Update);
            number = 1;
        }


        protected override void internal_Configurate()
        {
            if (number <= 0) throw new aConfigurateException(this, "Некорректное количество элементов для выборки");
        }


        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            base.internal_Write(CouponObject, writingValues);
            var resultValue = Input.value.AsQueryable<object>().Take<object>(number);
            Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
            return new aAtom[] { Result };
        }
    }

    //SUM
    [aBlock("Сумма", "Массивы", "Sum", "Sum", "Возвращает сумму последовательности", "Shirokov Ivan", 1, 2015, 08, 09)]
    public class ArraySum : aBlock
    {
        public aEnumerableAtom<double> Input { get; protected set; }
        public aEnumerableAtom<double> Result { get; protected set; }

        public ArraySum()
        {
            Input = new aEnumerableAtom<double>("inputArray", aAtomAccess.Write);
            Result = new aEnumerableAtom<double>("result", aAtomAccess.Update);
            
        }

        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            base.internal_Write(CouponObject, writingValues);
            var resultValue = Input.value.AsQueryable<double>().Sum();
            Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
            return new aAtom[] { Result };
        }
    }


    //SKIP
    [aBlock("пропуск элементов", "Массивы", "Skip", "Skip", "Пропускает элементы и возвращает оставшиеся", "Shirokov Ivan", 1, 2015, 08, 09)]
    public class ArraySkip : aBlock1
    {
        [cSetter("Число элементов", "Число элементов")]
        public int number { get; protected set; }

        public ArraySkip()
        {
            Input = new aEnumerableAtom<object>("inputArray", aAtomAccess.Write);
            Result = new aEnumerableAtom<object>("result", aAtomAccess.Update);
            number = 1;
        }

        protected override void internal_Configurate()
        {
            if (number <= 0) throw new aConfigurateException(this, "Некорректное количество элементов для пропуска");
        }

        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            base.internal_Write(CouponObject, writingValues);
            var resultValue = Input.value.AsQueryable<object>().Skip<object>(number);
            Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
            return new aAtom[] { Result };
        }
    }


    //SINGLE
    [aBlock("Конкретный элемент", "Массивы", "Single", "Single", "Возвращает конкретный элемент ", "Shirokov Ivan", 1, 2015, 08, 09)]
    public class ArraySingle : aBlock1
    {
        [cSetter("Номер элемента", "Номер элемента")]
        public int number { get; protected set; }

        public ArraySingle()
        {
            Input = new aEnumerableAtom<object>("inputArray", aAtomAccess.Write);
            Result = new aEnumerableAtom<object>("result", aAtomAccess.Update);
            number = 0;
        }

        protected override void internal_Configurate()
        {
            if (number < 0) throw new aConfigurateException(this, "Некорректный номер элемента");
        }

        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            base.internal_Write(CouponObject, writingValues);
            try
            {
                var resultValue = Input.value.AsQueryable<object>().Single<object>();
                Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
                return new aAtom[] { Result };
            }
            catch(aException)
            {
                return null;
            }
            
        }
    }


    //SELECT
    [aBlock("Проекция", "Массивы", "Select", "Select", "Проецирует элеенты в новую форму", "Shirokov Ivan", 1, 2015, 08, 09)]
    public class ArraySelect : aBlock1
    {

        [cSetter("Новая форма","Новая форма")]
        public string form {get; protected set;}
        
        public ArraySelect()
        {
            Input = new aEnumerableAtom<object>("inputArray", aAtomAccess.Write);
            Result = new aEnumerableAtom<object>("result", aAtomAccess.Update);
            form = "x => x*x";
        }


        protected override void internal_Configurate()
        {
            /*
             * Делаем проверку не пустое ли условие. В данном случае без паарметра Condition блок работать не может, 
             * поэтому если параметр не задан выкидываем ошибку с соответсвующим сообщением.
             */
            if (string.IsNullOrEmpty(form)) throw new aConfigurateException(this, "Не задана новая форма");
        }

        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            base.internal_Write(CouponObject, writingValues);
            var resultValue = Input.value.AsQueryable<object>().Select(form);
            Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
            return new aAtom[] { Result };
        }
    }

    //ORDERBY
    [aBlock("Сортировка", "Массивы", "orderBy", "orderBy", "Сортирует последовательность в порядке возрастания ключа", "Shirokov Ivan", 1, 2015, 08, 09)]
    public class ArrayOrderBy : aBlock1
    {
        public ArrayOrderBy()
        {
            Input = new aEnumerableAtom<object>("inputArray", aAtomAccess.Write);
            Result = new aEnumerableAtom<object>("result", aAtomAccess.Update);
        }


        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            base.internal_Write(CouponObject, writingValues);
            var resultValue = Input.value.AsQueryable<object>().OrderBy<object>("ascending");
            Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
            return new aAtom[] { Result };
        }
    }

    //REVERSE
    [aBlock("Обратный порядок", "Массивы", "Reverse", "Reverse", "Меняет порядок элементов на противоположный", "Shirokov Ivan", 1, 2015, 08, 09)]
    public class ArrayReverse : aBlock1
    {
        public ArrayReverse()
        {
            Input = new aEnumerableAtom<object>("inputArray", aAtomAccess.Write);
            Result = new aEnumerableAtom<object>("result", aAtomAccess.Update);
        }


        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            base.internal_Write(CouponObject, writingValues);
            var resultValue = Input.value.AsQueryable<object>().Reverse<object>();
            Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
            return new aAtom[] { Result };
        }
    }

    //MAX
    [aBlock("Максимальное значение", "Массивы", "Max", "Max", "Возвращает максимальный элемент", "Shirokov Ivan", 1, 2015, 08, 09)]
    public class ArrayMax : aBlock1
    {
        public ArrayMax()
        {
            Input = new aEnumerableAtom<object>("inputArray", aAtomAccess.Write);
            Result = new aEnumerableAtom<object>("result", aAtomAccess.Update);
        }


        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            base.internal_Write(CouponObject, writingValues);
            var resultValue = Input.value.AsQueryable<object>().Max<object>();
            Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
            return new aAtom[] { Result };
        }
    }

    //MIN
    [aBlock("Минимальное значение", "Массивы", "Min", "Min", "Возвращает минимальный элемент", "Shirokov Ivan", 1, 2015, 08, 09)]
    public class ArrayMin : aBlock1
    {
        public ArrayMin()
        {
            Input = new aEnumerableAtom<object>("inputArray", aAtomAccess.Write);
            Result = new aEnumerableAtom<object>("result", aAtomAccess.Update);
        }


        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            base.internal_Write(CouponObject, writingValues);
            var resultValue = Input.value.AsQueryable<object>().Min<object>();
            Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
            return new aAtom[] { Result };
        }
    }

    //LAST
    [aBlock("Последний элемент", "Массивы", "Last", "Last", "Возвращает последний элемент", "Shirokov Ivan", 1, 2015, 08, 09)]
    public class ArrayLast : aBlock1
    {

        public ArrayLast()
        {
            Input = new aEnumerableAtom<object>("inputArray", aAtomAccess.Write);
            Result = new aEnumerableAtom<object>("result", aAtomAccess.Update);
        }
       
        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            try
            {
                base.internal_Write(CouponObject, writingValues);
                var resultValue = Input.value.AsQueryable<object>().Last();
                Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
                return new aAtom[] { Result };
            }
            catch(aException)
            { return null; }
        }
    }

    //FIRST
    [aBlock("Первый элемент", "Массивы", "First", "First", "Возвращает первый элемент", "Shirokov Ivan", 1, 2015, 08, 09)]
    public class ArrayFirst : aBlock1
    {

        

        public ArrayFirst()
        {
            Input = new aEnumerableAtom<object>("inputArray", aAtomAccess.Write);
            Result = new aEnumerableAtom<object>("result", aAtomAccess.Update);
        }
        

        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            try
            {
                base.internal_Write(CouponObject, writingValues);
                var resultValue = Input.value.AsQueryable<object>().Last();
                Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
                return new aAtom[] { Result };
            }
            catch (aException)
            { return null; }
        }
    }

    //COUNT
    [aBlock("Количество элементов", "Массивы", "Count", "Count", "Возвращает количество элементов", "Shirokov Ivan", 1, 2015, 08, 09)]
    public class ArrayFirst : aBlock1
    {
        public ArrayFirst()
        {
            Input = new aEnumerableAtom<object>("inputArray", aAtomAccess.Write);
            Result = new aEnumerableAtom<object>("result", aAtomAccess.Update);
        }
        protected override aAtom[] internal_Write(object CouponObject, params aAtomXerocopy[] writingValues)
        {
            try
            {
                base.internal_Write(CouponObject, writingValues);
                var resultValue = Input.value.AsQueryable<object>().Count<object>();
                Result.Set(resultValue, Input.quality, Input.timeOfUpdate);
                return new aAtom[] { Result };
            }
            catch (aException)
            { return null; }
        }
    }


}
