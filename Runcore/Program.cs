﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Linq.Dynamic;
using a.Core.Scanning.Blocks;
using System.IO;
using a.Core;

namespace Runcore
{
    class Program
    {
        /// <summary>
        /// Тестовое приложение. вывод в консоли недавно созданные блоки.
        /// Это чисто для проверки что новые блок создался
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var date = DateTime.Parse("06.08.2015");
            aBlockFactory.theFactory.LoadDirrectory(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
            foreach(var supply in aBlockFactory.theFactory.Supplies.Where(s => s.Resources.FirstOrDefault( r => 
                {
                    if (r != null && (r.TypeDef as aBlockAttribute).releaseTime > DateTime.Parse("06.08.2015"))
                    {
                        return true;
                    }
                    else return false;
                }) != null ))
            {
                Console.WriteLine(supply.Name + ":");
                foreach(var b in supply.Resources)
                {
                    if ((b.TypeDef as aBlockAttribute).releaseTime > DateTime.Parse("06.08.2015"))
                    {
                        Console.WriteLine(string.Format("\tName:{0}; Type:{1}; Author:{2}", b.TypeDef.DisplayName, b.TypeDef.ResourceName, b.TypeDef.Author));
                    }
                }
            }
            Console.WriteLine("Press esc to quit...");
            while (Console.ReadKey().Key != ConsoleKey.Escape) { }
        }
    }
}
